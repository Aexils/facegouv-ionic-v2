import {Injectable} from '@angular/core';
import {Global} from '../global';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Resource} from '../models/resource';
import {Observable, Subject} from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {
  //#region subjects
  resourcesSubject = new Subject();
  resourceSubject = new Subject();
  numberResourcesNotValidedSubject = new Subject();
  //#endregion
  resources: Array<Resource>;
  resource: Resource;
  numberResourcesNotValided: number;

  constructor(private global: Global, private http: HttpClient, private router: Router,
              private toastr: ToastrService, private route: ActivatedRoute, private authservice: AuthService) {
  }

  async getResourcesByNumberAndDateOrder(): Promise<void> {
    const resourcesNumber = 20; // Nombre de ressources affichés sur la homepage
    const data = await this.http.get(`${this.global.apiUrl}resources/${resourcesNumber}`).toPromise();
    this.resourcesSubject.next(data as Array<Resource>);
  }

  async getResourceById(id: number): Promise<void> {
    const data = await this.http.get(`${this.global.apiUrl}resource/${id}`).toPromise();
    this.resourceSubject.next(data as Resource);
  }

  getAllResources(): Observable<any> {
    return this.http.get(`${this.global.apiUrl}resource`);
  }

  async getAllResourcesNotValided(): Promise<any> {
    const data = await this.http.get(`${this.global.apiUrl}resource_not_valided`).toPromise();
    this.numberResourcesNotValidedSubject.next(data['data']);
  }

  editResourceStateId(id, resourceStateId): void {
    const stateAdmin = JSON.parse(localStorage.getItem('currentUser'))['userStateId'];
    this.http.put(`${this.global.apiUrl}resource`, {id, resourceStateId, stateAdmin}).subscribe(() => {
      this.toastr.success('Ressource modifié avec succès');
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.erreur);
    });
  }

  async postResource(title, description, category, eventDate, eventPlace, image): Promise<void> {
    const body = {
      userId: this.authservice.user.id, category, title, description, eventDate, eventPlace, image
    };
    await this.http.post(`${this.global.apiUrl}resource/`, body).subscribe((data) => {
      this.toastr.success('Ressource créee avec succès !');
      this.router.navigate(['']);
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.erreur);
    });
  }
}
