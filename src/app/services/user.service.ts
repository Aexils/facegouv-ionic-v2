import {Global} from '../global';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  //#region subjects
  usersSubject = new Subject();
  //#endregion
  users: Array<User>;

  constructor(private global: Global, private http: HttpClient,
              private toastr: ToastrService) {
  }

  getAllUsers(): Observable<any> {
    return this.http.get(`${this.global.apiUrl}admin/all_users`);
  }

  getAllUsersExceptAdmin(): Observable<any> {
    return this.http.get(`${this.global.apiUrl}admin/all_users_except_admin`);
  }

  editUserStateId(id, userStateId): void {
    const stateAdmin = JSON.parse(localStorage.getItem('currentUser'))['userStateId'];
    this.http.put(`${this.global.apiUrl}admin/edit_user_state`, {id, userStateId, stateAdmin}).subscribe(() => {
      this.toastr.success('Un email a été envoyé à l\'utilisateur');
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.erreur);
    });
  }
}
