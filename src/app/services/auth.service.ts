import {Injectable} from '@angular/core';
import {User} from '../models/user';
import {BehaviorSubject} from 'rxjs';
import {Global} from '../global';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrTool} from '../tools/toastr-tool';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  user: User;

  EMAIL_REGEX = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
  PASSWORD_REGEX = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$/;

  constructor(private global: Global, private http: HttpClient, private router: Router,
              private toastr: ToastrTool, private route: ActivatedRoute) {
    this.currentUserSubject = new BehaviorSubject<User>(this.user);
    /*this.initLocalStorageUser();*/
  }

  async signIn(user: User, confirmPwd) {

    if (!this.EMAIL_REGEX.test(user.email)) {
      return await this.toastr.display('Ceci n\'est pas une adresse email', 'danger');
    }

    if (!this.PASSWORD_REGEX.test(user.password)) {
      return await this.toastr.display('Le mot de passe doit contenir au moins 6 caractères, 1 lettre et 1 chiffre', 'danger');
    }

    if (user.password === confirmPwd) {
      this.http.post(`${this.global.apiUrl}users/register/`, user).subscribe(async () => {
        await this.toastr.display('Inscription réussie, vous avez reçu un email afin d\'activer votre compte', 'success');
        await this.router.navigate(['/login'], {relativeTo: this.route});
      }, async (err: HttpErrorResponse) => {
        return await this.toastr.display(err.error.erreur, 'danger');
      });
    } else {
      return await this.toastr.display('Le mot de passe n\'est pas le même', 'danger');
    }
  }

  /*  login(email, password): void {
      const auth = {email, password};
      this.http.post(`${this.global.apiUrl}users/login`, auth).subscribe((token) => {
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
          })
        };
        this.http.get(`${this.global.apiUrl}users/protected`, httpOptions).subscribe((user) => {
          this.user = user['user'];
          localStorage.setItem('currentUser', JSON.stringify(this.user));
          this.toastr.success(`Bonjour ${this.user.firstName}`);
          this.router.navigate(['']);
          this.currentUserSubject?.next(this.user);
        }, (err: HttpErrorResponse) => {
          this.toastr.error(err.error.erreur);
        });
      }, (err: HttpErrorResponse) => {
        this.toastr.error(err.error.erreur);
      });
    }

    forgottenPassword(email): void {
      this.http.post(`${this.global.apiUrl}users/forgotten_password`, {email}).subscribe(() => {
        this.toastr.success('Un email vous a été envoyé');
        this.router.navigate(['/login']);
      });
    }

    editAccount(user: User): void {
      this.http.put(`${this.global.apiUrl}users/edit_account`, user).subscribe(() => {
        this.toastr.success('Profil mis à jour avec succès');
        this.router.navigate(['/profile']);
      }, (err: HttpErrorResponse) => {
        this.toastr.error(err.error.erreur);
      });
    }

    editPassword(password, newPassword, confirmPassword): void {
      if (!this.PASSWORD_REGEX.test(newPassword)) {
        this.toastr.error('Le mot de passe doit contenir au moins 6 caractères, 1 lettre et 1 chiffre');
        return;
      }

      if (newPassword === confirmPassword) {
        this.http.put(`${this.global.apiUrl}users/edit_password`, {
          id: this.user.id,
          password,
          newPassword
        }).subscribe(() => {
          this.toastr.success('Mot de passe mis à jour avec succès');
          this.router.navigate(['/profile']);
        }, (err: HttpErrorResponse) => {
          this.toastr.error(err.error.erreur);
        });
      } else {
        this.toastr.error('Le mot de passe n\'est pas le même');
        return;
      }
    }

    logout(): void {
      this.toastr.success(`À bientôt ${this.user.firstName}`);
      localStorage.removeItem('currentUser');
      this.router.navigate(['login']);
      this.currentUserSubject = null;
    }

    initLocalStorageUser(): void {
      const userLocal = localStorage.getItem('currentUser');
      if (userLocal && userLocal !== 'undefined') {
        this.user = JSON.parse(userLocal) as User;
        this.currentUserSubject.next(this.user);
      }
    }*/
}
