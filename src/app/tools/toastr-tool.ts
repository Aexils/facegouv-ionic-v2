import { ToastController } from '@ionic/angular';
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class ToastrTool {

  constructor(private toastr: ToastController) {}

  async display(msg: string, color: string, duration: number = 2000) {
    const toast = await this.toastr.create({
      message: msg,
      color: color,
      duration: duration
    });
    await toast.present();
  }
}
