import {UserStates} from './enums/user-states.enum';
import {IEditable} from './i-editable';

export class User implements IEditable {
  constructor() {

  }
  updatedAt: Date;
  createdAt: Date;
  id: number;

  lastName: string;
  firstName: string;
  password: string;
  age: number;
  email: string;
  tel: string;
  postalCode: string;
  userStateId: UserStates;
  isEnabled: boolean;
  profilePicture: Blob;

  toString(): string {
    return `${this.lastName} ${this.firstName}`;
  }


}
