export interface IEditable {
  id: number;
  createdAt: Date;
  updatedAt: Date;
}
