export enum UserStates {
  banned,
  visitor,
  verified,
  moderator,
  admin,
  superAdmin
}
