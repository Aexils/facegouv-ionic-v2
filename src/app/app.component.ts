import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Accueil', url: '/folder/Home', icon: 'home' },
  ];
  public appPagesAccount = [
    { title: 'Mes ressources', url: '/folder/resources/self', icon: 'document'},
  ]
  constructor() {}
}
